from django.db import models

# Create your models here.
class Contact(models.Model):
    name = models.CharField(max_length=100)
    email =models.CharField(max_length=100)
    phone =models.CharField(max_length=13)
    desc = models.TextField()
    date = models.DateField()

    def __str__(self):
        return self.name


class Payments(models.Model):
    user = models.CharField(max_length=100)
    amount = models.IntegerField(default=0)

    def __str__(self):
        return self.user

class employees(models.Model):
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    emp_id = models.IntegerField()

    def __str__(self):
        return self.firstname
    