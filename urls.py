from django.contrib import admin
from django.urls import path
from home import views
from rest_framework.urlpatterns import format_suffix_patterns
from home import views
from django.conf.urls import url

urlpatterns = [
    path("", views.index, name='home'),
    path("about", views.about, name='about'),
    path("services", views.services, name='services'),
    path("contact", views.contact, name='contact'),
    url(r'^employees/',views.employeeList.as_view())
]
