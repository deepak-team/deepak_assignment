from django.core.checks import messages
from django.shortcuts import redirect, render, HttpResponse
from datetime import datetime
from home.models import Contact, Payments, employees
from django.db import transaction
from django.contrib import messages
from home.serializers import employeesserializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import serializers, status
from django.shortcuts import get_object_or_404

# Create your views here.
def index(request):
    #return HttpResponse("this is hopepage of first app created by deepak agrahari")
    return render(request, 'index.html')

def about(request):
    #return HttpResponse("this is about page")
    return render(request, 'about.html')

def services(request):  
    if request.method == "POST":
        try:

            name1 = request.POST.get('name1')
            name2 = request.POST.get('name2')
            amount = request.POST.get('amount')

            with transaction.atomic():
                name1_obj = Payments.objects.get(user = name1)
                name1_obj.amount -= int(amount)
                name1_obj.save()
            

                name2_obj = Payments.objects.get(user = name2)
                name2_obj.amount += int(amount)
                name2_obj.save()
                messages.success(request, 'Payment is done')

        except Exception as e:
            print(e)
            messages.success(request, 'Something went wrong')
    #return HttpResponse("this is services page")
    return render(request, 'services.html')

def contact(request):
    if request.method == "POST":
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        desc = request.POST.get('desc')
        contact = Contact(name=name, email=email, phone=phone, desc=desc, date=datetime.today())
        contact.save()
    #return HttpResponse("this is contact page")
    return render(request, 'contact.html')


class employeeList(APIView):
    def get(self, request):
        employees1= employees.objects.all()
        serializer = employeesserializer(employees1, many=True)
        return Response(serializer.data)